//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.Video;

///// <summary>
///// Class to represent a game 'scene' corresponding to a tracked image, camera controlled by the AR camera
///// </summary>
//public class Page : MonoBehaviour
//{
//    [SerializeField]
//    public string pageName;            // used to match with tracked image name

//    [SerializeField]
//    private VideoPlayer pageIntroVideo;

//    [SerializeField]
//    private VideoPlayer pageOutroVideo;

//    [SerializeField]
//    private Camera pageCamera;

//    [SerializeField]
//    private LayerMask raycastLayers;

//    private float raycastDistance = 1000f;

//    [SerializeField]
//    // if true, page camera rotation set exactly as per AR camera, regardless of initial rotation
//    // if false, page camera rotates with AR camera, but relative to initial camera rotation
//    private bool mirrorARCamera = false;

//    [SerializeField]
//    private float ARCameraMoveFactor = 10f;      // to amplify AR camera movement

//    private Pose cameraStartPose;

//    [SerializeField]
//    private Light pageLight;

//    [SerializeField]
//    private AudioSource musicSource;

//    [SerializeField]
//    private AudioSource sfxSource;

//    public enum PageStatus
//    {
//        //IntroVideo,
//        InGame,
//        Completed,
//        //OutroVideo,
//    };

//    public PageStatus CurrentStatus { get; private set; }

//    public bool IsCompleted => CurrentStatus == PageStatus.Completed;

//    public bool IntroVideoPlaying => pageIntroVideo.isPlaying;
//    public bool OutroVideoPlaying => pageOutroVideo.isPlaying;
//    public bool IsPlayingVideo => IntroVideoPlaying || OutroVideoPlaying;


//    private void Start()
//    {
//        cameraStartPose.position = pageCamera.transform.position;
//        cameraStartPose.rotation = pageCamera.transform.rotation;
//    }

//    private void OnEnable()
//    {
//        GameEvents.OnARCameraPoseChanged += OnARCameraPoseChanged;
//        GameEvents.OnTouchOn += OnTouchOn;

//        pageIntroVideo.loopPointReached += OnIntroVideoEnd;
//        pageOutroVideo.loopPointReached += OnOutroVideoEnd;

//        SetStatus(PageStatus.InGame);
//    }

//    private void OnDisable()
//    {
//        GameEvents.OnARCameraPoseChanged -= OnARCameraPoseChanged;
//        GameEvents.OnTouchOn -= OnTouchOn;

//        pageIntroVideo.loopPointReached -= OnIntroVideoEnd;
//        pageOutroVideo.loopPointReached -= OnOutroVideoEnd;
//    }

//    private void OnIntroVideoEnd(VideoPlayer source)
//    {
//        pageIntroVideo.enabled = false;
//    }

//    private void OnOutroVideoEnd(VideoPlayer source)
//    {
//        pageOutroVideo.enabled = false;
//    }

//    private void Update()
//    {
//        if (CurrentStatus == PageStatus.InGame)
//            CameraRaycast();
//    }

//    public void SetStatus(PageStatus newStatus)
//    {
//        CurrentStatus = newStatus;

//        switch (newStatus)
//        {
//            //case PageStatus.IntroVideo:
//            //    if (pageIntroVideo.clip != null)
//            //        pageIntroVideo.Play();
//            //    break;

//            case PageStatus.InGame:
//                if (pageIntroVideo.clip != null)
//                {
//                    pageIntroVideo.enabled = true;
//                    pageIntroVideo.Play();
//                }
//                break;

//            case PageStatus.Completed:
//                if (pageOutroVideo.clip != null)
//                {
//                    pageOutroVideo.enabled = true;
//                    pageOutroVideo.Play();
//                }
//                break;

//            //case PageStatus.OutroVideo:
//            //    if (pageOutroVideo.clip != null)
//            //        pageOutroVideo.Play();
//            //    break;
//        }

//        //GameEvents.OnPageStatusChanged?.Invoke(this);     // TODO: needed?
//    }

//    // sync page camera to AR camera
//    private void OnARCameraPoseChanged(Pose ARCameraPose, Pose ARCameraPoseDelta)
//    {
//        if (IsPlayingVideo)
//            return;

//        // set page camera position relative to original position
//        // TODO: AR camera movement amplified?  buttons to move in 3D space?
//        pageCamera.transform.position = cameraStartPose.position + (ARCameraPoseDelta.position * ARCameraMoveFactor);  

//        if (mirrorARCamera)
//        {
//            pageCamera.transform.rotation = ARCameraPose.rotation;
//        }
//        else
//        {
//            // subtract rotation from start rotation.. like this...
//            pageCamera.transform.rotation = cameraStartPose.rotation * Quaternion.Inverse(ARCameraPoseDelta.rotation);

//            //// add AR camera rotation delta to page camera start rotation
//            //pageCamera.transform.rotation = cameraStartPose.rotation * ARCameraPoseDelta.rotation;
//        }
//    }

//    private void CameraRaycast()
//    {
//        if (CurrentStatus != PageStatus.InGame)
//            return;

//        if (IsPlayingVideo)
//            return;

//        //Vector2 screenCentre = pageCamera.ViewportToScreenPoint(new Vector2(0.5f, 0.5f));        // middle point of screen

//        // raycast to detect what camera centre is 'looking at'
//        RaycastHit hitInfo;
//        Physics.Raycast(pageCamera.transform.position, pageCamera.transform.forward, out hitInfo, raycastDistance, raycastLayers);

//        GameEvents.OnLookAtRaycast?.Invoke(hitInfo.transform);      // may be null!
//    }

//    private void OnTouchOn(Vector2 screenPosition)
//    {
//        if (CurrentStatus != PageStatus.InGame)
//            return;

//        if (IsPlayingVideo)
//            return;

//        //Vector2 touchPosition = pageCamera.ViewportToScreenPoint(screenPosition);
//        Ray ray = pageCamera.ScreenPointToRay(screenPosition);

//        // raycast to detect what was touched
//        RaycastHit hitInfo;
//        Physics.Raycast(ray, out hitInfo, raycastDistance, raycastLayers);
//        //Physics.Raycast(touchPosition, Vector3.forward, out hitInfo, raycastDistance, raycastLayers);

//        GameEvents.OnTouchRaycast?.Invoke(hitInfo.transform);           // may be null!
//    }
//}
