using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour
{
    [SerializeField]
    private float hoverDistance = 2f;
    [SerializeField]
    private float hoverTime = 2f;

    void Start()
    {
        StartHovering();
    }

    private void StartHovering()
    {
        Vector3 startPosition = transform.localPosition;

        LeanTween.moveY(gameObject, startPosition.y + hoverDistance, hoverTime)
                                    .setEaseOutSine()
                                    .setLoopPingPong();
    }
}
