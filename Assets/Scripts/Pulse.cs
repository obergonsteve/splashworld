using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulse : MonoBehaviour
{
    [SerializeField]
    private float pulseFactor = 2f;
    [SerializeField]
    private float pulseTime = 2f;

    // Start is called before the first frame update
    void Start()
    {
        StartPulsing();
    }

    private void StartPulsing()
    {
        Vector3 startScale = transform.localScale;

        LeanTween.scale(gameObject, new Vector3(startScale.x * pulseFactor, startScale.y * pulseFactor, startScale.z * pulseFactor), pulseTime)
                                    .setEaseOutBack()
                                    .setLoopPingPong();
    }
}
