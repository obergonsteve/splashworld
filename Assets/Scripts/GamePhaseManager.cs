//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;


///// <summary>
///// Handles transition between phases
///// </summary>
//public class GamePhaseManager : MonoBehaviour
//{
//    public enum GamePhase
//    {
//        Intro,                  // main menu
//        //ARRaycastPlanes,        // for cursor, object placement
//        ARImageTrack,           // track for printed book pages
//        InPageGame,             // in 3D game for current page (page camera, lights, audio, etc) - inc. intro/outro videos
//        GameOver                // no more pages!
//    };

//    public GamePhase CurrentPhase { get; private set; }

//    //public ImageTracking imageTracking;


//    // set phase according to in-game events
//    private void OnEnable()
//    {
//        //GameEvents.OnPlayPageForImage += OnPlayPageForImage;
//        GameEvents.OnPageGameStarted += OnPageGameStarted;
//        GameEvents.OnPageGameCompleted += OnPageGameCompleted;
//    }

//    private void OnDisable()
//    {
//        //GameEvents.OnPlayPageForImage -= OnPlayPageForImage;
//        GameEvents.OnPageGameStarted -= OnPageGameStarted;
//        GameEvents.OnPageGameCompleted -= OnPageGameCompleted;
//    }


//    private void Start()
//    {
//        SetPhase(GamePhase.ARImageTrack);        // Intro);
//    }


//    //private void OnPlayPageForImage(ARTrackedImage trackedImage)
//    //{
//    //    SetPhase(GamePhase.InPageGame);
//    //}

//    private void OnPageGameStarted(Page activatedPage, int pageIndex, int pageCount)
//    {
//        SetPhase(GamePhase.InPageGame);
//    }

//    private void OnPageGameCompleted(Page completedPage, int pageIndex, int pageCount, List<Page> completedPages)
//    {
//        SetPhase(GamePhase.ARImageTrack);        // track for next page image
//    }

//    private void SetPhase(GamePhase newPhase)
//    {
//        if (CurrentPhase == newPhase)
//            return;

//        bool changed = CurrentPhase != newPhase;

//        CurrentPhase = newPhase;

//        switch (CurrentPhase)
//        {
//            case GamePhase.Intro:
//                break;

//            //case GamePhase.ARRaycastPlanes:
//            //    imageTracking.enabled = true;
//            //    GameEvents.OnARRaycastActivate?.Invoke();
//            //    GameEvents.OnARImageTrackDeactivate?.Invoke();
//            //    break;

//            case GamePhase.ARImageTrack:
//                imageTracking.enabled = true;
//                GameEvents.OnARImageTrackActivate?.Invoke();
//                GameEvents.OnARRaycastActivate?.Invoke();
//                //GameEvents.OnARRaycastDeactivate?.Invoke();
//                break;

//            case GamePhase.InPageGame:
//                imageTracking.enabled = false;
//                GameEvents.OnARImageTrackDeactivate?.Invoke();
//                GameEvents.OnARRaycastDeactivate?.Invoke();
//                break;

//            case GamePhase.GameOver:
//                imageTracking.enabled = false;
//                GameEvents.OnGameOver?.Invoke();
//                GameEvents.OnARImageTrackDeactivate?.Invoke();
//                GameEvents.OnARRaycastDeactivate?.Invoke();
//                break;
//        }

//        GameEvents.OnGamePhaseChanged?.Invoke(CurrentPhase);
//        //Debug.Log("Game Phase set to: " + CurrentPhase.ToString());
//    }
//}
