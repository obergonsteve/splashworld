using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Camera arCamera;

    [SerializeField]
    private LayerMask raycastLayers;            // for 'touching' objects

    private float raycastDistance = 1000f;

    private bool syncingARCamera = false;       // fire camera pose event for other ('VR') cameras to sync to

    private Pose cameraStartPose;               // AR camera
    private Pose cameraCurrentPose;             // AR camera
    private Pose cameraPoseDelta;               // position and rotation of AR (main) camera - delta from cameraStartPose

    [SerializeField]
    private Light gameLight;                    // deactivated while in a splash world (own lights and cameras)

    //[SerializeField]
    //private AudioListener arAudioListener;

    public PageGame CurrentPageGame { get; private set; }


    void Start()
    {
        // capture start position of AR camera
        InitCamera();
        syncingARCamera = false;
    }

    private void OnEnable()
    {
        GameEvents.OnPageGameStatusChanged += OnPageGameStatusChanged;
        GameEvents.OnTouchOn += OnTouchOn;
    }

    private void OnDisable()
    {
        GameEvents.OnPageGameStatusChanged -= OnPageGameStatusChanged;
        GameEvents.OnTouchOn -= OnTouchOn;
    }


    private void Update()
    {
        if (syncingARCamera)       // AR camera is being used to control another 'VR' camera
        {
            cameraCurrentPose.position = arCamera.transform.position;
            cameraCurrentPose.rotation = arCamera.transform.rotation;

            // fire event to sync AR camera to eg. page camera
            //cameraPoseDelta.position = cameraCurrentPose.position + cameraStartPose.position;
            cameraPoseDelta.position = cameraCurrentPose.position - cameraStartPose.position;
            // add rotation to start rotation.. like this...
            //cameraPoseDelta.rotation = cameraCurrentPose.rotation * cameraStartPose.rotation;
            // subtract current rotation from start rotation.. like this...
            cameraPoseDelta.rotation = cameraStartPose.rotation * Quaternion.Inverse(cameraCurrentPose.rotation);

            GameEvents.OnARCameraPoseChanged?.Invoke(cameraCurrentPose, cameraPoseDelta);       // eg. move / rotate page camera
        }
        else      // eg. splash world camera not active
            PageGameRaycast();
    }

    private void PageGameRaycast()
    {
        if (CurrentPageGame != null && CurrentPageGame.CurrentStatus != PageGame.PageStatus.OnPageGame)
            return;

        // raycast to detect what camera centre is 'looking at'
        RaycastHit hitInfo;
        Physics.Raycast(arCamera.transform.position, arCamera.transform.forward, out hitInfo, raycastDistance, raycastLayers);

        GameEvents.OnPageGameLookAtRaycast?.Invoke(hitInfo.transform);      // may be null!
    }

    private void OnTouchOn(Vector2 screenPosition, int touchCount)
    {
        if (CurrentPageGame != null && CurrentPageGame.CurrentStatus != PageGame.PageStatus.OnPageGame)
            return;

        //Vector2 touchPosition = pageCamera.ViewportToScreenPoint(screenPosition);
        Ray ray = arCamera.ScreenPointToRay(screenPosition);

        // raycast to detect what was touched
        RaycastHit hitInfo;
        Physics.Raycast(ray, out hitInfo, raycastDistance, raycastLayers);
        //Physics.Raycast(touchPosition, Vector3.forward, out hitInfo, raycastDistance, raycastLayers);

        if (hitInfo.transform != null)
            Debug.Log("Page game touch: " + hitInfo.transform.name);

        GameEvents.OnPageGameTouchRaycast?.Invoke(hitInfo.transform);           // may be null!
    }


    private void OnPageGameStatusChanged(PageGame.PageStatus prevStatus, PageGame pageGame)
    {
        CurrentPageGame = pageGame;

        switch (pageGame.CurrentStatus)
        {
            case PageGame.PageStatus.Inactive:
            case PageGame.PageStatus.OnPageGame:
            case PageGame.PageStatus.Completed:
            default:
                //arAudioListener.enabled = true;
                gameLight.gameObject.SetActive(true);
                syncingARCamera = false;    // stop firing camera pose events
                break;

            case PageGame.PageStatus.InSplashWorld:
                // set camera start, to sync page game camera
                InitCamera();
                //arAudioListener.enabled = false;        // splashworld has own audio listener
                gameLight.gameObject.SetActive(false);
                syncingARCamera = true;     // fire camera pose events
                break;
        }
    }

    private void InitCamera()
    {
        // capture start position of AR camera
        cameraStartPose.position = arCamera.transform.position;      // AR camera
        cameraStartPose.rotation = arCamera.transform.rotation;      // AR camera
    }
}

