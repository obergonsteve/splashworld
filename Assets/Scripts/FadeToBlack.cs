using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FadeToBlack : MonoBehaviour
{
	public Image Blackout;
	public Color BlackOutColour = Color.black;

	public TextMeshProUGUI BlackoutText;

	private float fadeOutTime = 0.5f;          // to black
	private float fadeInTime = 0.25f;            // to clear

	private bool fadingToBlack = false;


	private void OnEnable()
	{
		GameEvents.OnFadeToBlack += FadeOut;
    }

	private void OnDisable()
	{
		GameEvents.OnFadeToBlack -= FadeOut;
	}

	// found out 'old scene'
	private void FadeOut(Action onFade = null, string message = "")
	{
		if (fadingToBlack)
			return;

		fadingToBlack = true;

		Blackout.color = Color.clear;
		Blackout.enabled = true;

		ScaleInText(message);

		LeanTween.alpha(Blackout.rectTransform, 1, fadeOutTime)
					.setEaseOutQuad()
					.setOnComplete(() =>
					{
						onFade?.Invoke();
						FadeIn();
					});
	}


	// fade in a new 'scene'
	private void FadeIn()
	{
		Blackout.color = BlackOutColour;
		Blackout.enabled = true;

		LeanTween.alpha(Blackout.rectTransform, 0, fadeInTime)
					.setEaseInQuad()
					.setOnComplete(() =>
					{
						Blackout.enabled = false;
						BlackoutText.text = "";
						fadingToBlack = false;
                    });
	}

	private void ScaleInText(string text)
	{
		if (text == "")
		{
			BlackoutText.text = text;
			return;
		}

		BlackoutText.transform.localScale = Vector3.zero;

		LeanTween.scale(BlackoutText.gameObject, Vector3.one, fadeOutTime)
					.setEaseOutElastic();
	}
}
