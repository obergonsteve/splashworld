using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;


[RequireComponent(typeof(ImageTargetBehaviour))]
[RequireComponent(typeof(DefaultObserverEventHandler))]
[RequireComponent(typeof(AudioSource))]

/// <summary>
/// Controls Image Tracking, (optional) on-page game and (optional) SplashWorld
/// </summary>
public class PageGame : MonoBehaviour
{
    [SerializeField]
    private bool hasOnPageGame = false;         // play game on-page?

    [SerializeField]
    private SplashWorld splashWorld;            // optional 'VR' world / game

    public bool HasSplashWorld => splashWorld != null;

    [Header("Audio")]

    [SerializeField]
    private AudioSource musicSource;            // looping

    [SerializeField]
    private AudioSource sfxSource;

    [SerializeField]
    private AudioClip gameMusic;

    [SerializeField]
    private AudioClip startGameAudio;

    [SerializeField]
    private AudioClip completeGameAudio;

    [Space]

    [SerializeField]
    private Transform exitObject;               // tap to complete page game        // TODO: temp!


    private DefaultObserverEventHandler targetEventHandler;
    private bool isTrackingImage = false;

    private ImageTargetBehaviour imageTargetBehaviour;

    public enum PageStatus
    {
        Inactive,                       // target image not been tracked
        OnPageGame,                     // playing on-page game
        InSplashWorld,                  // playing 'splash world' game
        Completed                       // game(s) played and completed
    }

    public PageStatus CurrentStatus { get; private set; }

    public bool CanRepeat = true;
    public int CompletionCount { get; private set; }


    private void Awake()
    {
        targetEventHandler = GetComponent<DefaultObserverEventHandler>();
        imageTargetBehaviour = GetComponent<ImageTargetBehaviour>();
    }

    private void OnEnable()
    {
        targetEventHandler.OnTargetFound.AddListener(OnTargetFound);
        targetEventHandler.OnTargetLost.AddListener(OnTargetLost);

        StartListeningForTouch();
    }

    private void OnDisable()
    {
        targetEventHandler.OnTargetFound.RemoveListener(OnTargetFound);
        targetEventHandler.OnTargetLost.RemoveListener(OnTargetLost);

        StopListeningForTouch();
    }

    // start listening for in-game touch events
    private void StartListeningForTouch()
    {
        GameEvents.OnPageGameTouchRaycast += OnPageGameTouchRaycast;
        GameEvents.OnSplashWorldTouchRaycast += OnSplashWorldTouchRaycast;
    }

    // stop listening for in-game touch events
    private void StopListeningForTouch()
    {
        GameEvents.OnPageGameTouchRaycast -= OnPageGameTouchRaycast;
        GameEvents.OnSplashWorldTouchRaycast -= OnSplashWorldTouchRaycast;
    }

    // TODO: don't think this is working (probably overridden by Vuforia?)
    public void EnableImageTargetting(bool enabled)
    {
        targetEventHandler.enabled = enabled;
        imageTargetBehaviour.enabled = enabled;
    }

    private void OnPageGameTouchRaycast(Transform touched)
    {
        // check if exit object was touched
        if (CurrentStatus == PageStatus.OnPageGame && exitObject != null && touched == exitObject)
        {
            if (HasSplashWorld)
            {
                EnterSplashWorld();
                EnableImageTargetting(false);
            }
            else
            {
                SetStatus(PageStatus.Completed);
                EnableImageTargetting(true);
            }
        }
    }

    private void OnSplashWorldTouchRaycast(Transform touched, bool isExit)
    {
        if (CurrentStatus == PageStatus.InSplashWorld && isExit)
        {
            SetStatus(PageStatus.Completed);
            EnableImageTargetting(true);

            GameEvents.OnSplashWorldExit?.Invoke(splashWorld, this);       // SplashWorldManager

            if (CanRepeat)   // TODO: repeat? 
                SetStatus(PageStatus.Inactive);
        }
    }


    // child AR objects are activated 'on-page'
    private void OnTargetFound()
    {
        if (CurrentStatus == PageStatus.Completed && ! CanRepeat)
        {
            isTrackingImage = false;
            //StopListeningForTouch();

            GameEvents.OnPageGameBlocked?.Invoke(this, "You've already Played this Page!!");
            return;
        }

        isTrackingImage = true;
        //StartListeningForTouch();

        if (hasOnPageGame)
        {
            SetStatus(PageStatus.OnPageGame);
        }
        else if (HasSplashWorld)
        {
            EnterSplashWorld();
        }
    }

    // child AR objects are deactivated 'on-page'
    private void OnTargetLost()
    {
        isTrackingImage = false;
        //StopListeningForTouch();

        Debug.Log("OnTargetLost: " + name);

        //SetStatus(PageStatus.Inactive);

        //if (splashWorld != null && CurrentStatus == PageStatus.InSplashWorld)
        //{
        //    GameEvents.OnSplashWorldExit?.Invoke(splashWorld, this);        // SplashWorldManager
        //}
    }

    private void SetStatus(PageStatus newStatus)
    {
        if (CurrentStatus == newStatus)
            return;

        var prevStatus = CurrentStatus;
        CurrentStatus = newStatus;

        switch (CurrentStatus)
        {
            case PageStatus.Inactive:
                break;

            case PageStatus.OnPageGame:
                PlaySFX(startGameAudio);
                PlayMusic();
                break;

            case PageStatus.InSplashWorld:
                StopMusic();
                break;

            case PageStatus.Completed:
                if (! HasSplashWorld)        // has own audio
                    PlaySFX(completeGameAudio);

                StopMusic();

                CompletionCount++;
                break;

            default:
                break;
        }

        GameEvents.OnPageGameStatusChanged?.Invoke(prevStatus, this);
    }

    private void EnterSplashWorld()
    {
        if (HasSplashWorld)
        {
            SetStatus(PageStatus.InSplashWorld);
            GameEvents.OnSplashWorldEnter?.Invoke(splashWorld, this);       // SplashWorldManager
        }
    }

    private void PlayMusic()
    {
        if (musicSource != null && gameMusic != null)
        {
            musicSource.loop = true;
            musicSource.clip = gameMusic;
            musicSource.Play();
        }
    }

    private void StopMusic()
    {
        if (musicSource != null && gameMusic != null)
        {
            musicSource.Stop();
        }
    }

    private void PlaySFX(AudioClip sfxAudio)
    {
        if (sfxSource != null && sfxAudio != null)
        {
            sfxSource.clip = sfxAudio;
            sfxSource.Play();
        }
    }
}
