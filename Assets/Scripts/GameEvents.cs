using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents
{
    // GameManager

    public delegate void OnARCameraPoseChangedDelegate(Pose cameraPose, Pose ARCameraPoseDelta);
    public static OnARCameraPoseChangedDelegate OnARCameraPoseChanged;

    // PageGame

    public delegate void OnPageGameStatusChangedDelegate(PageGame.PageStatus prevStatus, PageGame page);
    public static OnPageGameStatusChangedDelegate OnPageGameStatusChanged;

    //public delegate void OnPageGameCompletedDelegate(PageGame page, int completionCount);
    //public static OnPageGameCompletedDelegate OnPageGameCompleted;

    public delegate void OnPageGameBlockedDelegate(PageGame page, string reason);
    public static OnPageGameBlockedDelegate OnPageGameBlocked;

    public delegate void OnPageGameLookAtRaycastDelegate(Transform lookingAt);
    public static OnPageGameLookAtRaycastDelegate OnPageGameLookAtRaycast;

    public delegate void OnPageGameTouchRaycastDelegate(Transform touched);
    public static OnPageGameTouchRaycastDelegate OnPageGameTouchRaycast;

    // SplashWorld

    public delegate void OnSplashWorldEnterDelegate(SplashWorld splashWorld, PageGame page);
    public static OnSplashWorldEnterDelegate OnSplashWorldEnter;

    public delegate void OnSplashWorldExitDelegate(SplashWorld splashWorld, PageGame page);
    public static OnSplashWorldExitDelegate OnSplashWorldExit;

    public delegate void OnSplashWorldLookAtRaycastDelegate(Transform lookingAt);
    public static OnSplashWorldLookAtRaycastDelegate OnSplashWorldLookAtRaycast;

    public delegate void OnSplashWorldTouchRaycastDelegate(Transform touched, bool isExit);
    public static OnSplashWorldTouchRaycastDelegate OnSplashWorldTouchRaycast;

    // Touch Input

    public delegate void OnTouchOnDelegate(Vector2 screenPosition, int touchCount);
    public static OnTouchOnDelegate OnTouchOn;

    public delegate void OnTouchMoveDelegate(Vector2 screenPosition, int touchCount, Vector2 direction, TimeSpan timeFromStart);
    public static OnTouchMoveDelegate OnTouchMove;

    public delegate void OnTouchOffDelegate(Vector2 screenPosition, int touchCount, Vector2 direction, TimeSpan timeFromStart);
    public static OnTouchOffDelegate OnTouchOff;


    // UI

    public delegate void OnFadeToBlackDelegate(Action onFade, string message);
    public static OnFadeToBlackDelegate OnFadeToBlack;

}
