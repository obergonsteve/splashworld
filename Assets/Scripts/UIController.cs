using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class UIController : MonoBehaviour
{
    public TextMeshProUGUI pageGameStatus;
    public TextMeshProUGUI pageCompletionCount;
    public TextMeshProUGUI playerPrompt;
    public TextMeshProUGUI lookingAtText;
    public TextMeshProUGUI touchedObjectText;

    private void OnEnable()
    {
        GameEvents.OnPageGameStatusChanged += OnPageGameStatusChanged;
        //GameEvents.OnPageGameCompleted += OnPageGameCompleted;
        GameEvents.OnPageGameBlocked += OnPageGameBlocked;

        GameEvents.OnPageGameLookAtRaycast += OnPageGameLookAtRaycast;
        GameEvents.OnSplashWorldLookAtRaycast += OnSplashWorldLookAtRaycast;
        GameEvents.OnPageGameTouchRaycast += OnPageGameTouchRaycast;
        GameEvents.OnSplashWorldTouchRaycast += OnSplashWorldTouchRaycast;
    }

    private void OnDisable()
    {
        GameEvents.OnPageGameStatusChanged -= OnPageGameStatusChanged;
        //GameEvents.OnPageGameCompleted -= OnPageGameCompleted;
        GameEvents.OnPageGameBlocked -= OnPageGameBlocked;

        GameEvents.OnPageGameLookAtRaycast -= OnPageGameLookAtRaycast;
        GameEvents.OnSplashWorldLookAtRaycast -= OnSplashWorldLookAtRaycast;
        GameEvents.OnPageGameTouchRaycast -= OnPageGameTouchRaycast;
        GameEvents.OnSplashWorldTouchRaycast -= OnSplashWorldTouchRaycast;
    }


    private void Start()
    {
        pageGameStatus.text = "Tracking...";
        playerPrompt.text = "Scan Book Page!";
        pageCompletionCount.text = "";
        lookingAtText.text = "";
        touchedObjectText.text = "";
    }


    private void OnPageGameStatusChanged(PageGame.PageStatus prevStatus, PageGame page)
    {
        switch (page.CurrentStatus)
        {
            case PageGame.PageStatus.Inactive:
                pageGameStatus.text = " Tracking...";
                playerPrompt.text = "Scan Book Page!";
                lookingAtText.text = "";
                touchedObjectText.text = "";
                pageCompletionCount.text = "";
                break;

            // start of on-page game
            case PageGame.PageStatus.OnPageGame:
                pageGameStatus.text = " Play Page Game!";
                playerPrompt.text = "Tap to Complete On-Page Game!";
                lookingAtText.text = "";
                touchedObjectText.text = "";
                pageCompletionCount.text = "Completed: " + page.CompletionCount + (page.CompletionCount == 1 ? " time" : " times");
                break;

            // optional
            case PageGame.PageStatus.InSplashWorld:
                pageGameStatus.text = " Play Splash World!";
                playerPrompt.text = "Tap to Complete Splash World!";
                break;

            case PageGame.PageStatus.Completed:
                pageGameStatus.text = " Page Game Completed!";
                playerPrompt.text = "Scan Book Page!";
                lookingAtText.text = "";
                touchedObjectText.text = "";
                pageCompletionCount.text = "Completed: " + page.CompletionCount + (page.CompletionCount == 1 ? " time" : " times");
                break;
        }
    }

    private void OnPageGameBlocked(PageGame page, string reason)
    {
        pageGameStatus.text = reason;
    }

    private void OnPageGameLookAtRaycast(Transform lookingAt)
    {
        if (lookingAt != null)
            lookingAtText.text = "Looking at: " + lookingAt.name;
        else
            lookingAtText.text = "";
    }

    private void OnPageGameTouchRaycast(Transform touched)
    {
        if (touched != null)
            touchedObjectText.text = "Touched: " + touched.name;
        else
            touchedObjectText.text = "";
    }

    private void OnSplashWorldLookAtRaycast(Transform lookingAt)
    {
        if (lookingAt != null)
            lookingAtText.text = "Looking at: " + lookingAt.name;
        else
            lookingAtText.text = "";
    }

    private void OnSplashWorldTouchRaycast(Transform touched, bool isExit)
    {
        if (touched != null)
            touchedObjectText.text = "Touched: " + touched.name;
        else
            touchedObjectText.text = "";
    }
}
