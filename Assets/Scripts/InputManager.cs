using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Handle screen touches
/// Mouse clicks emulate touches
/// </summary>
/// 
public class InputManager : MonoBehaviour
{
    private Vector2 touchStartPosition;
    private DateTime touchStartTime;

    void Update()
    {
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                touchStartPosition = Input.GetTouch(0).position;
                touchStartTime = DateTime.Now;

                GameEvents.OnTouchOn?.Invoke(touchStartPosition, 1);
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                var touchPosition = Input.GetTouch(0).position;
                GameEvents.OnTouchMove?.Invoke(touchPosition, 1, touchPosition - touchStartPosition, DateTime.Now - touchStartTime);
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                var touchPosition = Input.GetTouch(0).position;
                GameEvents.OnTouchOff?.Invoke(touchPosition, 1, touchPosition - touchStartPosition, DateTime.Now - touchStartTime);
            }
        }

        // emulate touch with left mouse
        else if (Input.GetMouseButtonDown(0))
        {
            GameEvents.OnTouchOn?.Invoke(Input.mousePosition, 1);
        }
        else if (Input.GetMouseButton(0))
        {
            GameEvents.OnTouchMove?.Invoke(Input.mousePosition, 1, (Vector2)Input.mousePosition - touchStartPosition, DateTime.Now - touchStartTime);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            GameEvents.OnTouchOff?.Invoke(Input.mousePosition, 1, (Vector2)Input.mousePosition - touchStartPosition, DateTime.Now - touchStartTime);
        }

        // right mouse == 2 finger swipe
        else if (Input.GetMouseButtonDown(1))
        {
            GameEvents.OnTouchOn?.Invoke(Input.mousePosition, 2);
        }
        else if (Input.GetMouseButton(1))
        {
            GameEvents.OnTouchMove?.Invoke(Input.mousePosition, 2, (Vector2)Input.mousePosition - touchStartPosition, DateTime.Now - touchStartTime);
        }
        else if (Input.GetMouseButtonUp(1))
        {
            GameEvents.OnTouchOff?.Invoke(Input.mousePosition, 2, (Vector2)Input.mousePosition - touchStartPosition, DateTime.Now - touchStartTime);
        }
    }
}
