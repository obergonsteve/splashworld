using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    public float spinSpeed = 100f;

    public enum SpinAxis { SpinX, SpinY, SpinZ }
    public SpinAxis SpinAround = SpinAxis.SpinY;

    void Update()
    {
        switch (SpinAround)
        {
            case SpinAxis.SpinX:
                transform.Rotate(Time.deltaTime * spinSpeed, 0f, 0f);
                break;

            case SpinAxis.SpinY:
                transform.Rotate(0f, Time.deltaTime * spinSpeed, 0f);
                break;

            case SpinAxis.SpinZ:
                transform.Rotate(0f, 0f, Time.deltaTime * spinSpeed);
                break;
        }
    }
}
