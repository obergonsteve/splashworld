using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PageManager : MonoBehaviour
{
    [SerializeField]
    private List<PageGame> pages = new List<PageGame>();

    private void OnEnable()
    {
        GameEvents.OnPageGameStatusChanged += OnPageGameStatusChanged;
    }

    private void OnDisable()
    {
        GameEvents.OnPageGameStatusChanged -= OnPageGameStatusChanged;
    }

    private void OnPageGameStatusChanged(PageGame.PageStatus prevStatus, PageGame page)
    {
        // disable image targeting if completed
        //page.EnableImageTargetting(page.CurrentStatus != PageGame.PageStatus.Completed);
    }
}
