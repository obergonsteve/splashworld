using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashWorldManager : MonoBehaviour
{
    //[SerializeField]
    //private List<SplashWorld> splashWorlds = new List<SplashWorld>();

    private SplashWorld currentSplashWorld;

    private void OnEnable()
    {
        GameEvents.OnSplashWorldEnter += OnSplashWorldEnter;
        GameEvents.OnSplashWorldExit += OnSplashWorldExit;
    }

    private void OnDisable()
    {
        GameEvents.OnSplashWorldEnter -= OnSplashWorldEnter;
        GameEvents.OnSplashWorldExit -= OnSplashWorldExit;
    }

    private void OnSplashWorldEnter(SplashWorld splashWorld, PageGame page)
    {
        currentSplashWorld = splashWorld;
        GameEvents.OnFadeToBlack?.Invoke(ActivateSplashWorld, "Welcome to SplashWorld!");

        //ActivateSplashWorld(splashWorld);
    }

    private void OnSplashWorldExit(SplashWorld splashWorld, PageGame page)
    {
        if (splashWorld != currentSplashWorld)      // WTF!!
        {
            Debug.LogError("OnSplashWorldExit: " + splashWorld.name + " not current!");
            return;
        }

        GameEvents.OnFadeToBlack?.Invoke(DeactivateSplashWorld, "Congratulations!");        // completed splashworld

        //DeactivateSplashWorld();
    }

    private void ActivateSplashWorld()
    {
        if (currentSplashWorld == null)
            return;

        currentSplashWorld.gameObject.SetActive(true);
    }

    private void DeactivateSplashWorld()
    {
        if (currentSplashWorld == null)
            return;

        currentSplashWorld.gameObject.SetActive(false);
        currentSplashWorld = null;
    }
}
